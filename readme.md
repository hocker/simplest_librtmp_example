# 最简单的基于LibRTMP的例子

Simplest LibRTMP Example

-----
@ hocker

雷神 将这个样例公开，我这里把这个样例移植到linux上，工大家参考和使用！！

工程在linux目录下， 雷神 的[原工程](https://gitee.com/leixiaohua1020/simplest_librtmp_example)没有改动以对雷神的尊敬！！

-----


雷霄骅，张晖

leixiaohua1020@126.com

zhanghuicuc@gmail.com

中国传媒大学/数字电视技术

Communication University of China / Digital TV Technology

http://blog.csdn.net/leixiaohua1020

-----

本工程包含了LibRTMP的使用示例，包含如下子工程：

- simplest_librtmp_receive: 接收RTMP流媒体并在本地保存成FLV格式的文件。
- simplest_librtmp_send_flv: 将FLV格式的视音频文件使用RTMP推送至RTMP流媒体服务器。
- simplest_librtmp_send264: 将内存中的H.264数据推送至RTMP流媒体服务器。

-----
This Solutions contains some examples about usage of LibRTMP.

It contains following projects:

- simplest_librtmp_receive: Receive RTMP streams and save as FLV file.
- simplest_librtmp_send_flv: Stream FLV file to RTMP streaming server.
- simplest_librtmp_send264: Stream H.264 raw data to RTMP streaming server.
